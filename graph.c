#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "graph.h"

/**
 * Pridani vrcholu do grafu
 *
 * @param id id vrcholu ktery chceme pridat
 * @param g graf do ktereho pridavame
 */
int add_vertex(int id, struct Graph g) {
    g.vertexes[id].id = id;
    g.vertexes[id].color = -1;
    g.vertexes[id].n = 0;
    return 0;
}

/**
 * Pridani souseda k vrcholu
 *
 *  @param id id vrcholu kteremu prirazujeme souseda
 *  @param id_neighbor id souseda k prirazeni
 *  @param g graf do ktereho pridavame
 */
int add_vertex_neighbor(int id, int id_neighbor, struct Graph g) {
    if (g.vertexes[id].n == 0) {
        g.vertexes[id].neighbors = malloc(sizeof(struct Vertex) * 1000);
        if(g.vertexes[id].neighbors == NULL){
            return 2;
        }
    }
    g.vertexes[id].neighbors[g.vertexes[id].n].id = id_neighbor;
    g.vertexes[id].neighbors[g.vertexes[id].n].color = -1;
    g.vertexes[id].neighbors[g.vertexes[id].n].neighbors = NULL;
    g.vertexes[id].n++;
    return 0;
}

/**
 * Vytvoreni grafu
 *
 * @param n_vertexes pocet vrcholu grafu
 */
struct Graph create_graph(int n_vertexes) {
    int i;
    struct Graph g;
    g.n = n_vertexes;
    g.vertexes = malloc(sizeof(struct Vertex) * (n_vertexes + 1));
    for (i = 0; i < n_vertexes + 1; i++) {
        g.vertexes[i].id = 0;
        g.vertexes[i].color = -1;
        g.vertexes[i].n = 0;
        g.vertexes[i].neighbors = NULL;
    }
    return g;
}

/**
 * Vytisknuti grafu do konzole
 *
 * @param g graf ktery chceme vytisknout
 * @param frequencies pole s frekvencemi
 */
int print_graph(struct Graph g, char **frequencies) {
    if (check_graph(g) == 0) {
        int i;
        for (i = 0; i < g.n; i++) {
            printf("%d %s", g.vertexes[i].id, frequencies[g.vertexes[i].color]);
        }
    }
    else {
        return 3;
    }
    return 0;
}

/**
 * Funkce zkontroluje jestli ve vytvorenem grafu nedochazi k nejake kolizi
 *
 * @param g graf ktery chceme zkontrolovat
 *
 * @return 3 pokud se v grafu nachazi kolize
 */
int check_graph(struct Graph g) {
    int i, j;
    for (i = 0; i < g.n; i++) {
        for (j = 0; j < g.vertexes[i].n; j++) {
            if (g.vertexes[i].color == g.vertexes[g.vertexes[i].neighbors[j].id].color) {
                return 3;
            }
        }
    }
    return 0;
}

/**
 * Obarveni grafu - prideleni frekvenci jednotlivym vysilacum
 *
 * @param g graf ktery chceme obarvit
 * @param available_freq kolik frekvenci mame k dispozici
 *
 * @return 3 pokud pri obarvovani potrebujeme vice frekvenci nez mame
 */
int color_graph(struct Graph g, int available_freq) {
    struct Stack *stack = create_stack();
    int i, j, min_color, id;

    /** Prochazime graf a pokud nalezneme neobarveny vrchol, vlozime ho do zasobniku */
    for (i = g.n; i > 0; i--) {
        if (g.vertexes[i].color == -1) {
            if (push(g.vertexes[i].id, stack) == 5)return 5;
        }
        /** Dokud neni zasobnik prazdny */
        while (stack->top > -1) {
            id = stack->items[stack->top];
            pop(stack);
            min_color = get_lowest_freq(g, id, available_freq); /** Priradime frekvenci */
            g.vertexes[id].color = min_color;
            if (min_color >= available_freq) { /** Pokud nemame dostatek frekvenci vratime 3*/
                return 3;
            }
            /** Pokud nektery ze sousedu aktualniho vrcholu je neobarveny a neni v zasobniku, pridame do zasobniku */
            for (j = g.vertexes[id].n - 1; j >= 0; j--) {
                if ((g.vertexes[g.vertexes[id].neighbors[j].id].color == -1) &&
                    (is_not_in_stack(g.vertexes[id].neighbors[j].id, stack))) {
                    if (push(g.vertexes[id].neighbors[j].id, stack) == 5)return 5;
                }
            }
        }
    }
    free_stack(stack);
    return 0;
}

/**
 * Funkce vracejici frekvenci pro vysilac
 *
 * @param g graf ve kterem vrchol lezi
 * @param id id vrcholu kteremu chceme priradit barvu
 * @param available_freq dostupne frekvence
 *
 * @return frekvenci pro vysilac
 */
int get_lowest_freq(struct Graph g, int id, int available_freq) {
    int i, j, min_color, is_free;
    is_free = 0;
    min_color = available_freq - 1;
    for (i = available_freq - 1; i >= 0; i--) {
        for (j = 0; j < g.vertexes[id].n; j++) {
            if (g.vertexes[g.vertexes[id].neighbors[j].id].color == i) {
                is_free = 0;
                break;
            }
            else {
                is_free = 1;
            }
        }
        if ((i <= min_color) && (is_free)) {
            min_color = i;
        }
    }
    return min_color;

}

/**
 * Uvolni pamet
 *
 * @param g graf
 */
int free_graph(struct Graph g) {
    int i;
    for(i = 0; i<g.n;i++){
        free(g.vertexes[i].neighbors);
    }
    free(g.vertexes);
    return 0;
}
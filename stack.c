#include <stdlib.h>
#include "stack.h"
#define STACK_SIZE 1000

/**
 * Funkce pridavajici do zasobniku
 *
 * @param id id vrcholu
 * @param ps ukazatel na zasobnik
 *
 * @return 5 pokud zasobnik pretece
 */
int push(int id, struct Stack *ps) {
    if (ps->top == STACK_SIZE - 1) {
        return 5;
    } else {
        ps->items[++(ps->top)] = id;
        return 0;
    }
}

/**
 * Vybere ze zasobniku
 *
 * @param ps ukazatel na zasobnik
 */
int pop(struct Stack *ps) {
    return (ps->items[(ps->top)--]);
}

/**
 * Vytvoreni zasobniku
 */
struct Stack *create_stack(){
    int i;
    struct Stack *stack = malloc(sizeof(struct Stack)*132);
    stack->top=0;
    for(i=0;i<STACK_SIZE;i++){
        stack->items[i]=0;
    }
    return stack;
}

/*
 * Uvolni pamet zasobniku
 *
 * @param ps ukazatel na zasobnik
 */
int free_stack(struct Stack *ps){
    free(ps);
    return 0;
}

/**
 * Kontroluje zda se id nachazi v zasobniku
 *
 * @param id id vrcholu
 * @param s ukazatel na zasobnik
 *
 * @return 0 pokud prvek v zasobniku je
 */
int is_not_in_stack(int id, struct Stack *s){
    int i;
    for (i = 0; i<s->top;i++){
        if(s->items[i]==id){return 0;}
    }
    return 0;
}